# Basic Filesystem SPA

## Requirements
- Node.js 8.11.x
- npm 6.0.x

## Setup
```sh
> npm install
```

## Run
Starts the app on `localhost:3000`
```sh
> npm start
```
