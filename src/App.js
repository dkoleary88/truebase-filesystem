import React, { Component } from 'react';
import uuid from 'uuid/v4';
import { ToolBar, ContentList } from './components';

const styles = {
  app: {
    display: 'flex',
    flexDirection: 'column',
    padding: 40,
  },
  display: {
    padding: 10,
    margin: 10,
    height: 400,
    display: 'flex',
    flexDirection: 'row',
    borderWidth: 1,
    borderColor: '#888',
    borderStyle: 'solid',
  },
};

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      objects: [{
        id: 'root',
        parent: null,
        type: 'folder',
        name: 'root',
      }],
      selected: 'root',
    };
    this.addObject = this.addObject.bind(this);
    this.deleteObject = this.deleteObject.bind(this);
    this.renameObject = this.renameObject.bind(this);
    this.selectObject = this.selectObject.bind(this);
    this.getObjectById = this.getObjectById.bind(this);
    this.getFolder = this.getFolder.bind(this);
    this.buildPath = this.buildPath.bind(this);
    this.buildLists = this.buildLists.bind(this);
  }

  getObjectById(id) {
    const { objects } = this.state;
    return objects.filter(o => o.id === id)[0];
  }

  getFolder(id) {
    const obj = this.getObjectById(id);
    if (obj.type === 'folder') {
      return obj;
    }
    return this.getObjectById(obj.parent);
  }

  selectObject(id) {
    this.setState({ selected: id });
  }

  addObject({ type, name }) {
    const { objects, selected } = this.state;
    const selectedObj = this.getObjectById(selected);
    const parent = selectedObj.type === 'folder' ? selectedObj.id : selectedObj.parent;
    const id = uuid();
    this.setState({
      objects: objects.concat([{
        id,
        type,
        name,
        parent,
      }]),
    });
  }

  deleteObject(id) {
    const { parent } = this.getObjectById(id);
    const { objects } = this.state;
    const newObjs = objects.filter(o => o.id !== id && o.parent !== id);
    this.setState({
      objects: newObjs,
      selected: parent,
    });
  }

  renameObject(id, name) {
    const { objects } = this.state;
    const newObjs = objects.map((o) => {
      if (o.id === id) {
        return { ...o, name };
      }
      return o;
    });
    this.setState({ objects: newObjs });
  }

  buildPath(selectedObj) {
    let current = selectedObj;
    const path = [current];
    while (current.id !== 'root') {
      const parent = this.getObjectById(current.parent);
      path.unshift(parent);
      current = parent;
    }
    return path;
  }

  buildLists(selected) {
    const { objects } = this.state;
    const selectedObj = this.getObjectById(selected);
    const path = this.buildPath(selectedObj);

    return path
      .filter(o => o.type === 'folder')
      .map(({ id }, i, arr) => {
        let highlightId;
        if (i === arr.length - 1) {
          highlightId = selectedObj.type === 'file' ? selectedObj.id : 'list';
        } else {
          highlightId = arr[i + 1].id;
        }
        const list = objects.filter(o => o.parent === id);
        return (
          <ContentList
            parent={id}
            highlightId={highlightId}
            key={id}
            list={list}
            selectObject={this.selectObject}
          />
        );
      });
  }

  render() {
    const { selected } = this.state;
    const { name: folderName } = this.getFolder(selected);
    const selectedObj = this.getObjectById(selected);
    return (
      <div style={styles.app}>
        <ToolBar
          selectedObj={selectedObj}
          folderName={folderName}
          addObject={this.addObject}
          deleteObject={this.deleteObject}
          renameObject={this.renameObject}
        />
        <div style={styles.display}>
          {this.buildLists(selected)}
        </div>
      </div>
    );
  }
}

export default App;
