import React from 'react';
import PropTypes from 'prop-types';
import fileIcon from '../assets/file-icon.png';
import folderIcon from '../assets/folder-icon.png';

const styles = {
  list: {
    margin: 5,
    width: 200,
    border: '1px solid #999',
    display: 'flex',
    flexDirection: 'column',
    height: '100%',
    zIndex: 1,
  },
  item: {
    display: 'flex',
    flexDirection: 'row',
    width: '100%',
    zIndex: 10,
  },
  icon: {
    height: 22,
    width: 22,
  },
  highlight: {
    backgroundColor: '#CAEAFF',
  },
};

const ListItem = ({ object, selectObject, highlight }) => {
  const { type, name, id } = object;
  const itemStyle = highlight ? { ...styles.item, ...styles.highlight } : styles.item;
  const onItemClick = (e) => {
    e.stopPropagation();
    selectObject(id);
  };
  return (
    <div style={itemStyle} onClick={onItemClick}>
      <img src={type === 'folder' ? folderIcon : fileIcon} alt={type} style={styles.icon} />
      <div>{name + (type === 'folder' ? '  >' : '')}</div>
    </div>
  );
};

ListItem.propTypes = {
  object: PropTypes.objectOf(PropTypes.string).isRequired,
  selectObject: PropTypes.func.isRequired,
  highlight: PropTypes.bool.isRequired,
};

const ContentList = ({
  parent, list, selectObject, highlightId,
}) => {
  const listStyle = highlightId === 'list' ? { ...styles.list, ...styles.highlight } : styles.list;
  return (
    <div style={listStyle} onClick={() => selectObject(parent)}>
      {list.map((object) => {
      const highlight = object.id === highlightId;
      return (
        <ListItem
          key={object.id}
          object={object}
          selectObject={selectObject}
          highlight={highlight}
        />
      );
    })}
    </div>
  );
};

ContentList.propTypes = {
  parent: PropTypes.string.isRequired,
  list: PropTypes.arrayOf(PropTypes.object).isRequired,
  selectObject: PropTypes.func.isRequired,
  highlightId: PropTypes.string.isRequired,
};

export default ContentList;
