import React, { Component } from 'react';
import PropTypes from 'prop-types';

const initialState = {
  name: '',
  type: 'folder',
  rename: '',
};

class ToolBar extends Component {
  constructor() {
    super();
    this.state = initialState;
    this.handleInputChange = this.handleInputChange.bind(this);
    this.handleAddObj = this.handleAddObj.bind(this);
    this.handleRename = this.handleRename.bind(this);
  }

  handleInputChange(event) {
    return this.setState({
      [event.target.name]: event.target.value,
    });
  }

  handleAddObj() {
    const { name, type } = this.state;
    this.setState(initialState);
    this.props.addObject({ name, type });
  }

  handleRename() {
    const { id } = this.props.selectedObj;
    const { rename } = this.state;
    this.setState(initialState);
    this.props.renameObject(id, rename);
  }

  render() {
    const { folderName, selectedObj, deleteObject } = this.props;
    const { name, type, rename } = this.state;
    return (
      <div className="ToolBar">
        <select name="type" value={type} onChange={this.handleInputChange}>
          <option value="file">file</option>
          <option value="folder">folder</option>
        </select>
        <input name="name" value={name} onChange={this.handleInputChange} placeholder={`${type} name`} />
        <button onClick={this.handleAddObj} disabled={!name}>Add {type} to {folderName}</button>

        <button onClick={() => deleteObject(selectedObj.id)} disabled={selectedObj.id === 'root'}>Delete {selectedObj.id === 'root' ? '' : selectedObj.name}</button>

        <input name="rename" value={rename} onChange={this.handleInputChange} placeholder={`Enter new name for ${selectedObj.type} ${selectedObj.name}`} />
        <button onClick={this.handleRename} disabled={selectedObj.id === 'root'}> Rename {selectedObj.id === 'root' ? '' : selectedObj.name }</button>
      </div>
    );
  }
}

ToolBar.propTypes = {
  addObject: PropTypes.func.isRequired,
  deleteObject: PropTypes.func.isRequired,
  renameObject: PropTypes.func.isRequired,
  selectedObj: PropTypes.objectOf(PropTypes.string).isRequired,
  folderName: PropTypes.string.isRequired,
};

export default ToolBar;
